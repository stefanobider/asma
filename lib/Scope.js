const fs = require('fs');
const path = require('path');
const Key = require('./Key');
var console = require('./logger').child('Scope.js');

function Scope(filePath, name) {
    this.allKeys = {};
    this.path = filePath;
    this.name = name || path.basename(filePath, '.json');;
    this.status = 'created',
    this.lastCheck = 0,
    this.lastSync = 0;
    this.lastMod = 0;
    this.cacheMaxTime = 5*60*1000;

    this.public = {};
    this.public.key = this.key.bind(this);
    this.public.keys = this.keys.bind(this);
    this.public.setCacheTime = this.setCacheTime.bind(this);
}

Scope.prototype.setCacheTime = function(time) {
    console.log('setting cache time to ' + time + ' for '+ this.name);
    this.cacheMaxTime = time;
    return this;
}

Scope.prototype.keys = function() {
    return this.allKeys;
}

Scope.prototype.key = function(key) {
    if(!this.allKeys[key]) this.allKeys[key] = new Key(key, this);
    return this.allKeys[key];
}

Scope.prototype.add = function(key, value) {
    return this.key(key).setValue(value);
}

var s = 0;

Scope.prototype.sync = function() {
    console.log('sync')

    var internals = ++s;

    var self = this;
    return new Promise((resolve, reject) => {
        self.checkFile()
        .then(() => {
            if(self.lastSync && self.lastMod && self.lastMod < self.lastSync) {
                console.log(internals+ ' sync : not updating');
                return resolve();
            }
            console.log(internals+ ' sync : updating...');
            self.lastSync = new Date().getTime();

            var values = fs.readFile(self.path, (err, values) => {
                console.log(internals+ ' sync : read from file')
                if(err) {
                    console.log(internals+ ' sync : error reading from file', e);
                    self.status = 'error';
                    return resolve();
                }

                try { values = JSON.parse(values) }
                catch(e) {
                    console.log(internals+ ' sync : error parsing JSON');
                    return reject();
                }

                console.log(internals+ ' sync : setting values...');

                for(var key in values) {
                    self.key(key).setValue(values[key]);
                }

                resolve()
            });


        })
        .catch(resolve)
    })
}

Scope.prototype.checkCache = function() {
    var now = new Date().getTime();

    if(this.status == 'created') return;

    if((now - this.lastCheck) < this.cacheMaxTime ) {
        console.log('scope : cached', now-this.lastCheck, ' vs ', this.cacheMaxTime);
        return;
    }

    console.log('scope : synching')
    this.sync();
}

Scope.prototype.checkFile = function() {
    console.log('check');
    this.lastCheck = new Date().getTime();
    var self = this;
    self.status = 'checking';
    return new Promise((resolve, reject) => {
        console.log('check : opening file...');
        fs.stat(self.path, (err, stats) => {
            if(err) {
                console.log('check : file does not exist');
                self.status = 'void';
                return reject();
            }

            console.log('check : file exists');
            self.lastMod = new Date(stats.ctime).getTime();
            self.status = 'exists';
            resolve()
        })
    })
}


module.exports = Scope;
