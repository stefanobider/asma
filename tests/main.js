var Settings = require('../lib/Settings');
const fs = require('fs');

var filepath1 = process.cwd() + '/tests/files/one-scope.json';

before(function() {
    fs.writeFileSync(filepath1, JSON.stringify({
       "setting-1" : 1,
       "setting-2" : "2",
       "setting-3" : "1,2,3"
   }));
})

describe('Defaults', function() {

    it('should create a scope without file, add key with default value and retrieve that value', function(done) {
        var settings = new Settings();
        settings.addScope('some.json');
        settings.scope('some').key('a').default(1);
        var value = settings.scope('some').key('a').value();
        if(value != 1) return done(new Error('Wrong value: ' + value))
        done();
    })

    it('should create a scope without file, add key with default value and transform it', function(done) {
        var settings = new Settings();
        settings.addScope('some.json');
        settings.scope('some').key('a').default('1,2,4').transform(v => v.split(','));

        var value = settings.scope('some').key('a').value();
        if(!Array.isArray(value)) return done(new Error('Did not transform value'));
        if(value.join(',') != '1,2,4') return done(new Error('Transformation was not correct'));
        done();
    })

    it('should create a scope with file and retrieve all values', function(done) {
        var settings = new Settings();
        settings.addScope(filepath1);
        
        settings.sync()
        .then(() => {
            var setting1 = settings.scope('one-scope').key('setting-1');
            var setting2 = settings.scope('one-scope').key('setting-2');
            var setting3 = settings.scope('one-scope').key('setting-3');

            if(!setting1.valueSet()) return done(new Error('setting-1 was not set'))
            if(!setting2.valueSet()) return done(new Error('setting-2 was not set'))
            if(!setting3.valueSet()) return done(new Error('setting-3 was not set'))
            if(setting1.value() != '1') return done(new Error('setting-1 value was wrong: ' + setting1.value()))
            if(setting2.value() != 2) return done(new Error('setting-2 value was wrong: ' + setting2.value()))
            if(setting3.value() != '1,2,3') return done(new Error('setting-3 value was wrong: ' + setting3.value()))
            done();
        })
        .catch(done)
    })

    it('should create a scope with file, set defaults and transformations before and after synching and get the corret values', function(done) {
        var settings = new Settings();
        settings.addScope(filepath1);
        settings.scope('one-scope').key('setting-1').default('AAA');

        settings.sync()
        .then(() => {
            settings.scope('one-scope').key('setting-2').default('BBB');
            settings.scope('one-scope').key('setting-3').default('5,6,7').transform(v => v.split(','));

            var setting1 = settings.scope('one-scope').key('setting-1');
            var setting2 = settings.scope('one-scope').key('setting-2');
            var setting3 = settings.scope('one-scope').key('setting-3');

            if(!setting1.valueSet()) return done(new Error('setting-1 was not set'))
            if(!setting2.valueSet()) return done(new Error('setting-2 was not set'))
            if(!setting3.valueSet()) return done(new Error('setting-3 was not set'))
            if(setting1.value() != '1') return done(new Error('setting-1 value was wrong: ' + setting1.value()))
            if(setting2.value() != 2) return done(new Error('setting-2 value was wrong: ' + setting2.value()))
            if(!Array.isArray(setting3.value())) return done(new Error('setting-3 was not transformed: ' + setting3.value()))
            if(setting3.value().join(',') != '1,2,3') return done(new Error('setting-3 value was wrong: ' + setting3.value()))
            done();
        })
        .catch(done)
    })


})
