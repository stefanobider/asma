const Scope = require('./Scope');
var console = require('./logger').child('Settings.js');

function Settings(scope){
    var self = {};
    self.scopes = {};
    self.numScopes = 0;

    this.addScope = function(path, name) {
        var scope = new Scope(path, name);
        if(self.scopes[scope.name] !== undefined) {
            throw new Error('ASMa: a scope with name ' + scope.name + ' already exists');
        }

        self.scopes[scope.name] = scope;
        self.numScopes++;
        return this;
    }

    this.scope = function(name) {
        return self.scopes[name].public;
    }

    this.key = function(key) {
        if(self.numScopes != 1) throw new Error('ASMa: cannot call key() on settings object if multiple scopes exist');
        for(var x in self.scopes) return self.scopes[x].key(key);
        return null;
    }

    this.sync = function() {
        console.log('calling main sync')
        var promises = [];
        for(var x in self.scopes) promises.push(self.scopes[x].sync());
        return Promise.all(promises);
    }

}

module.exports = Settings;
