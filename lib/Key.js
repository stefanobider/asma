const Juta = require('juta');
var console = require('./logger').child('Key.js');

function Key(key, parentScope) {
    var self = {};
    self.key = key;
    self.value = null;
    self.valueRaw = null;
    self.valueTransformed = null;
    self.valueSet = false;
    self.isDefault = false;
    self.defaultValue = null;
    self.transformFn = null;
    self.parentScope = parentScope;

    this.is = function(compare) {
        return this.value() == compare;
    }

    this.value = function() {
        self.parentScope.checkCache();
        return self.value;
    }

    this.valueSet = function() {
        return self.valueSet;
    }

    this.isDefault = function() {
        return self.isDefault;
    }

    this.default = function(value) {
        self.defaultValue = value;
        if(!self.valueSet) this.setValue(value);
        self.isDefault = true;
        return this;
    }

    this.transform = function(fn) {
        self.transformFn = fn;
        if(self.valueSet) this.setValue(self.valueRaw);
        return this;
    }

    this.setValue = function(value) {
        var finalValue = value;
        self.valueRaw = value;

        if(typeof self.transformFn == 'function') {
            finalValue = self.valueTransformed = self.transformFn(value);
        }

        self.valueSet = true;
        self.value = finalValue;
        self.isDefault = false;
        return this;
    }
}


module.exports = Key;
